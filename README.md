# dblsqd Node.js SDK

This module provides a simple Promise-based API for retrieving and parsing
[DBLSQD](https://www.dblsqd.com) Feeds.

## Which DBLSQD module should I use?

This module is useful for adding update notifications and/or auto-updates to
any Node.js application.

**If you are using Electron, you can also use the drop-in auto-update UI** built on
top of this module, [dblsqd-electron](https://www.npmjs.com/package/dblsqd-electron).

If you need access to the full API of DBLSQD (e. g. for integrating DBLSQD into
deployment tools), check out the [dblsqd-api](https://www.npmjs.com/package/dblsqd-api)
module.

## Installing

You can install *dblsqd-sdk* via npm. Add *dblsqd-sdk* to your `package.json` or
install it via the command-line: `npm i --save dblsqd-sdk`

## Usage

Use *dblsqd-sdk* in your application like this:

```javascript
const {Feed} = require("dblsqd-sdk")

let feed = new Feed("https://feeds.dblsqd.com/:app_token", ":channel", ":os", ":arch")
feed.load().then(() => {
    //Get all available releases
    const releases = feed.getReleases()

    //Only get updates to the current version
    const {version} = require("./package.json")
    const updates = feed.getUpdates(version)

    //Download an update
    if (updates.length > 0) {
        feed.downloadRelease(updates[0].version).then(file => {
            console.log(`The update was successfully downloaded to ${file}`)
        })
    }
})
```