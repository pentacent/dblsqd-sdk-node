"use strict"
const url = require("url")
const http = require("http")
const https = require("https")
const fs = require("fs")
const path = require("path")
const os = require("os")
const semver = require("semver")

const autoArch = () => {
    switch (process.arch) {
        case "ia32": return "x86"
        case "x64": return "x86_64"
        case "ppc64": return "ppc_64"
        default: return process.arch
    }
}

const autoOs = () => {
    switch (process.platform) {
        case "darwin": return "mac"
        case "win32": return "win"
        default: return process.platform
    }
}

/**
 * Class for interacting with a DBLSQD Feed.
 * @class Feed
 * @param {string} baseUrl - Base URL (i. e. https://feeds.dblsqd.com/:app_token)
 * @param {string} [channel] - Feed channel (e. g. "release" or "stable")
 * @param {string} [os] - Feed operating system (e. g. "win" or "linux")
 * @param {string} [arch] - Feed processor architecture (e. g. "x86_64")
 * @param {string} [type] - Artifact type
 */
function Feed(baseUrl, channel, os, arch, type) {
    channel = channel ? channel : "release"
    arch = arch ? arch : autoArch()
    os = os ? os : autoOs()
    type = type ? "?t=" + type : ""
    let urlComponents = [baseUrl, channel, os, arch, type]
    this.url = url.parse(urlComponents.join("/"))
}

/**
 * Sets feed URL.
 * Calling this method overrides the URL created by the constructor.
 * @memberof Feed
 * @function setUrl
 * @param {string} url - Full Feed URL
 */
Feed.prototype.setUrl = function(feedUrl) {
    this.url = url.parse(feedUrl)
}

const makeRequest = url => url.protocol == "https:" ?
    https.request(url) :
    http.request(url)


/**
 * Loads a feed from the server.
 * @memberof Feed
 * @function load
 * @returns {Promise}
 */
Feed.prototype.load = function() {
    if (this.data) return Promise.resolve(this.data)
    if (this.promise) return this.promise
    
    this.promise = new Promise((resolve, reject) => {
        let request = makeRequest(this.url)
        let result = ""
        request.on("response", response => {
            response.on("data", data => result += data)
            response.on("end", () => resolve(result))
        })
        request.on("error", error => reject(error))
        request.setTimeout(10000)
        request.end()
    }).then(json => this.data = JSON.parse(json))

    return this.promise
}

const getTempWriteStream = function(filename) {
    let extRegexp = /(?:\.tar)?\.[a-zA-Z0-9]+$/
    let extIndex = filename.search(extRegexp)
    extIndex = extIndex > -1 ? extIndex : filename.length
    let filenameTemplate = [
        filename.substr(0, extIndex),
        "-XXXXXX",
        filename.substr(extIndex)
    ].join("")
    let fileHandle
    for (let i = 0; i < 100; i++) {
        let filePath = path.join(
            os.tmpdir(),
            filenameTemplate.replace("XXXXXX", i)
        )
        try {
            fileHandle = fs.openSync(filePath, "wx")
            return {
                filePath: filePath,
                file: fs.createWriteStream(null, {fd: fileHandle})
            }
        } catch (e) {
            if (e.code !== "EEXIST") throw e
        }
    }
}

const getUrlFilename = function(url) {
    let pathnameSegments = url.pathname.split("/")
    return pathnameSegments[pathnameSegments.length - 1]
}

const getResponseLength = function(response) {
    let contentLength = response.headers["content-length"]
    return contentLength ? parseInt(contentLength) : -1
}


/**
 * This callback provides progress information during the download of a release.
 * @callback Feed~downloadCallback
 * @param {number} bytesReceived
 * @param {number} bytesTotal
 */

/**
 * Downloads a release to a temporary file.
 * @memberof Feed
 * @function downloadRelease
 * @param {string} version - version number.
 * @param {Feed~downloadCallback} [progressCallback]
 * @returns {Promise.<filename>}
 */
Feed.prototype.downloadRelease = function(version, progressCallback) {
    return new Promise((resolve, reject) => {
        if (!this.data) return reject("Feed must be loaded first")
        
        let downloadUrl = this.data.releases
            .filter(release => release.version === version)
            .map(release => release.download.url)
            [0]
        if (!downloadUrl) return reject(`Release ${version} not found`)
        downloadUrl = url.parse(downloadUrl)
        let baseName = getUrlFilename(downloadUrl)
        let {file, filePath} = getTempWriteStream(baseName)
        let request = makeRequest(downloadUrl)

        request.on("response", response => {
            let bytesTotal = getResponseLength(response)
            let progressCallbackInterval = progressCallback ?
                setInterval(() => {
                    progressCallback.call(this, file.bytesWritten, bytesTotal)
                },  200) :
                undefined

            let fileFinish = false, responseEnd = false
            response.pipe(file)
            file.on("finish", () => {
                file.close()
                fileFinish = true
                if (progressCallback) {
                    clearInterval(progressCallbackInterval)
                    progressCallback.call(this, file.bytesWritten, bytesTotal)
                }
                if (responseEnd) {
                    resolve(filePath)
                }
            })
            response.on("end", () => {
                responseEnd = true
                if (fileFinish) {
                    resolve(filePath)
                }
            })
        })
        request.on("error", error => {
            file.close()
            fs.unlinkSync(file.path)
            reject(error)
        })
        request.end()
    })
}

/**
 * Returns all releases from the Feed.
 * @memberof Feed
 * @function getReleases
 * @returns {Object[]}
 */
Feed.prototype.getReleases = function() {
    if (!this.data) return
    return this.data.releases
}

/**
 * Returns all releases from the Feed that are newer than `version`.
 * @memberof Feed
 * @function getUpdates
 * @param {string} version - current version
 * @param {string} [maxVersion] - highest version allowed
 * @returns {Object[]}
 */
Feed.prototype.getUpdates = function(version, maxVersion) {
    if (!this.data) return
    return this.data.releases
        .filter(release => semver.gt(release.version, version))
        .filter(release => !maxVersion || semver.lte(release.version, maxVersion))
}

module.exports = Feed